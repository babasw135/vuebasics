export default {
    REMOVE_TOKEN: 'authenticated/removeToken',
    FETCH_TOKEN: 'authenticated/fetchToken',
    FETCH_NAME: 'authenticated/fetchName',

    GET_NAME: 'authenticated/getName',

    DO_LOGIN: 'authenticated/doLogin',
    DO_LOGOUT: 'authenticated/doLogout',
    DO_PASSWORD_CHANGE: 'authenticated/doPasswordChange',
    DO_PROFILE: 'authenticated/doProfile'
}
